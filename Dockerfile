FROM node:21.7.3-alpine

WORKDIR /app
COPY . .

COPY package.json package-lock.json ./

RUN apk --no-cache add --virtual builds-deps build-base python3 && \
    npm install && \
    apk del builds-deps

EXPOSE 3000
EXPOSE $PORT

ENTRYPOINT [ "npm", "start"]
